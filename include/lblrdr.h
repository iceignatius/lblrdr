#ifndef _LABEL_RENDERER_H_
#define _LABEL_RENDERER_H_

#ifdef __cplusplus
#   include <string>
#endif

#include <fontrdr/fontrdr.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Font flags
 */
enum lblrdr_font_flags
{
    LBLRDR_REGULAR      = FONTRDR_REGULAR,      ///< Normal fonts with all defaults
    LBLRDR_BOLD         = FONTRDR_BOLD,         ///< Enable bold style
    LBLRDR_OBLIQUE      = FONTRDR_OBLIQUE,      ///< Enable oblique or Italic style
    LBLRDR_MONOSPACE    = FONTRDR_MONOSPACE,    ///< Enable monospace
    LBLRDR_UNDERLINE    = FONTRDR_UNDERLINE,    ///< Enable underline
};

/**
 * Layout flags
 */
enum lblrdr_layout_flags
{
    LBLRDR_TAB_INDENT           = 0x0000,   ///< Tab intent mode
    LBLRDR_TAB_AS_SPACE         = 0x0100,   ///< Treat tab as a space character
    LBLRDR_TAB_MASK             = 0x0100,
    LBLRDR_TABSIZE_MASK         = 0x00FF,

    LBLRDR_ALIGN_LEFT           = 0x0000,   ///< Align lines left
    LBLRDR_ALIGN_RIGHT          = 0x0200,   ///< Align lines right
    LBLRDR_ALIGN_CENTRE         = 0x0400,   ///< Align lines centre
    LBLRDR_ALIGN_MASK           = 0x0600,

    LBLRDR_SOFTWRAP             = 0x1000,   ///< Enable the soft-wrap function
    LBLRDR_SOFTWRAP_ANYWHERE    = 0x2000,   ///< Allow soft-wrap not on spaces only
    LBLRDR_SOFTWRAP_MASK        = 0x3000,
};

/**
 * @class lblrdr
 * @brief Label renderer
 */
typedef struct lblrdr
{
    // WARNING: All variables are private!

    fontrdr_t *fonts;
    unsigned font_size;
    int font_flags;
    int layout_flags;
    uint32_t fg_colour;
    uint32_t bg_colour;

} lblrdr_t;

void lblrdr_init(lblrdr_t *self);
void lblrdr_destroy(lblrdr_t *self);

int lblrdr_add_font_files(lblrdr_t *self, const char *wildcard);
void lblrdr_clear_font_files(lblrdr_t *self);
int lblrdr_get_font_size(const lblrdr_t *self);
void lblrdr_set_font_size(lblrdr_t *self, unsigned h);
int lblrdr_get_font_flags(const lblrdr_t *self);
void lblrdr_set_font_flags(lblrdr_t *self, int flags);
int lblrdr_set_font_cache(lblrdr_t *self, unsigned num);
void lblrdr_clear_font_cache(lblrdr_t *self);
fontrdr_t* lblrdr_get_fonts(lblrdr_t *self);
void lblrdr_set_fonts(lblrdr_t *self, fontrdr_t *fonts);

int lblrdr_get_layout_flags(const lblrdr_t *self);
void lblrdr_set_layout_flags(lblrdr_t *self, int flags);
int lblrdr_get_tab_size(const lblrdr_t *self);
void lblrdr_set_tab_size(lblrdr_t *self, int n);
uint32_t lblrdr_get_fg(const lblrdr_t *self);
void lblrdr_set_fg(lblrdr_t *self, uint32_t colour);
uint32_t lblrdr_get_bg(const lblrdr_t *self);
void lblrdr_set_bg(lblrdr_t *self, uint32_t colour);

uimg_t* lblrdr_render(lblrdr_t *self, const char *str, int max_width);

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef __cplusplus

/**
 * LBLRDR encapsulate class
 */
class LabelRenderer : protected lblrdr_t
{
public:
    LabelRenderer() { lblrdr_init(this); }
    ~LabelRenderer() { lblrdr_destroy(this); }

    LabelRenderer(const LabelRenderer &src) = delete;
    LabelRenderer& operator=(const LabelRenderer &src) = delete;

public:
    int AddFontFiles(const std::string &wildcard) { return lblrdr_add_font_files(this, wildcard.c_str()); } ///< Same as lblrdr_add_font_files()
    void ClearFontFiles() { lblrdr_clear_font_files(this); }                    ///< Same as lblrdr_clear_fonts()
    int GetFontSize() const { return lblrdr_get_font_size(this); }              ///< Same as lblrdr_get_font_size()
    void SetFontSize(unsigned h) { lblrdr_set_font_size(this, h); }             ///< Same as lblrdr_set_font_size()
    int GetFontFlags() const { return lblrdr_get_font_flags(this); }            ///< Same as lblrdr_get_font_flags()
    void SetFontFlags(int flags) { lblrdr_set_font_flags(this, flags); }        ///< Same as lblrdr_set_font_flags()
    int SetFontCache(unsigned num) { return lblrdr_set_font_cache(this, num); } ///< Same as lblrdr_set_font_cache()
    void ClearFontCache() { lblrdr_clear_font_cache(this); }                    ///< Same as lblrdr_clear_font_cache()
    fontrdr_t* GetFonts() { return lblrdr_get_fonts(this); }                    ///< Same as lblrdr_get_fonts()
    void SetFonts(fontrdr_t *fonts) { lblrdr_set_fonts(this, fonts); }          ///< Same as lblrdr_set_fonts()

    int GetLayoutFlags() const { return lblrdr_get_layout_flags(this); }        ///< Same as lblrdr_get_layout_flags()
    void SetLayoutFlags(int flags) { lblrdr_set_layout_flags(this, flags); }    ///< Same as lblrdr_set_layout_flags()
    int GetTabSize() const { return lblrdr_get_tab_size(this); }                ///< Same as lblrdr_get_tab_size()
    void SetTabSize(int n) { lblrdr_set_tab_size(this, n); }                    ///< Same as lblrdr_set_tab_size()
    uint32_t GetFg() const { return lblrdr_get_fg(this); }                      ///< Same as lblrdr_get_fg()
    void SetFg(uint32_t colour) { lblrdr_set_fg(this, colour); }                ///< Same as lblrdr_set_fg()
    uint32_t GetBg() const { return lblrdr_get_bg(this); }                      ///< Same as lblrdr_get_bg()
    void SetBg(uint32_t colour) { lblrdr_set_bg(this, colour); }                ///< Same as lblrdr_set_bg()

    Uimg Render(const std::string &str, int max_width = -1)
    {
        /// Same as lblrdr_render()
        return lblrdr_render(this, str.c_str(), max_width);
    }
};

#endif  // __cplusplus

#endif
