#ifndef _LBLRDR_UTF8_H_
#define _LBLRDR_UTF8_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

const char* lblrdr_utf8_decode(const char *str, uint32_t *code);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
