#include <uimg/uimgblit.h>
#include "lblrdr_utf8.h"
#include "glyphlist.h"
#include "lblrdr.h"

void lblrdr_init(lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Constructor
     */
    self->fonts = NULL;
    self->font_size = 32;
    self->font_flags = FONTRDR_REGULAR;
    self->layout_flags =
        ( 8 & LBLRDR_TABSIZE_MASK ) | LBLRDR_TAB_INDENT | LBLRDR_ALIGN_LEFT;
    self->fg_colour = UIMG_COLOUR_WHITE;
    self->bg_colour = UIMG_COLOUR_BLANK;
}

void lblrdr_destroy(lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Destructor
     */
    if(self->fonts)
        fontrdr_release(self->fonts);
}

int lblrdr_add_font_files(lblrdr_t *self, const char *wildcard)
{
    /**
     * @memberof lblrdr
     * @brief Load and append fonts from files
     *
     * @param self      Object instance
     * @param wildcard  Name of file(s) to be load, wildcard is available
     * @return ZERO if success, and others indicates an error occurred.
     *
     * @remarks
     * The sequence to load font files will be the same when searching a code point
     */
    if(!self->fonts)
        self->fonts = fontrdr_create();
    if(!self->fonts)
        return ENOMEM;

    return fontrdr_add_fonts(self->fonts, wildcard);
}

void lblrdr_clear_font_files(lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Pop out the loaded font files
     */
    if(self->fonts)
        fontrdr_clear_fonts(self->fonts);
}

int lblrdr_get_font_size(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current font size
     */
    return self->font_size;
}

void lblrdr_set_font_size(lblrdr_t *self, unsigned h)
{
    /**
     * @memberof lblrdr
     * @brief Set font size
     *
     * @param self  Object instance
     * @param h     The new font size (font height in pixels)
     */
    self->font_size = h;
}

int lblrdr_get_font_flags(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current font flags
     */
    return self->font_flags;
}

void lblrdr_set_font_flags(lblrdr_t *self, int flags)
{
    /**
     * @memberof lblrdr
     * @brief Set font flags
     *
     * @param self  Object instance
     * @param flags The new font flags in lblrdr_font_flags
     */
    self->font_flags = flags;
}

int lblrdr_set_font_cache(lblrdr_t *self, unsigned num)
{
    /**
     * @memberof lblrdr
     * @brief Set font cache number
     *
     * @param self  Object instance
     * @param num   The new cache number
     * @return ZERO if success, and others indicates an error occurred.
     */
    if(!self->fonts)
        self->fonts = fontrdr_create();
    if(!self->fonts)
        return ENOMEM;

    fontrdr_set_cache_num(self->fonts, num);
    return 0;
}

void lblrdr_clear_font_cache(lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Clear font cache
     */
    if(self->fonts)
        fontrdr_clear_cache(self->fonts);
}

fontrdr_t* lblrdr_get_fonts(lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get the current using font module
     */
    if(!self->fonts)
        self->fonts = fontrdr_create();

    return self->fonts;
}

void lblrdr_set_fonts(lblrdr_t *self, fontrdr_t *fonts)
{
    /**
     * @memberof lblrdr
     * @brief Assign to use another font module
     * @details
     * This function allows multiple renderers to share the same font module.
     * Simplify the fonts setup process, and
     * saving memory by share the same cache.
     *
     * @param self  Object instance
     * @param fonts The new font module to be use.
     *              The current font module will be released then
     *              be replaced by this.
     */
    if(self->fonts)
    {
        fontrdr_release(self->fonts);
        self->fonts = NULL;
    }

    if(fonts)
        self->fonts = fontrdr_shadow(fonts);
}

int lblrdr_get_layout_flags(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current layout flags
     */
    return self->layout_flags;
}

void lblrdr_set_layout_flags(lblrdr_t *self, int flags)
{
    /**
     * @memberof lblrdr
     * @brief Set layout flags
     *
     * @param self  Object instance
     * @param flags The new layout flags in ::lblrdr_layout_flags
     */
    self->layout_flags = flags;
}

int lblrdr_get_tab_size(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current tab size
     */
    return self->layout_flags & LBLRDR_TABSIZE_MASK;
}

void lblrdr_set_tab_size(lblrdr_t *self, int n)
{
    /**
     * @memberof lblrdr
     * @brief Set tab size
     *
     * @param self  Object instance
     * @param n     Tab size in number of spaces
     */
    self->layout_flags &= ~LBLRDR_TABSIZE_MASK;
    self->layout_flags |= n & LBLRDR_TABSIZE_MASK;
}

uint32_t lblrdr_get_fg(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current foreground colour
     */
    return self->fg_colour;
}

void lblrdr_set_fg(lblrdr_t *self, uint32_t colour)
{
    /**
     * @memberof lblrdr
     * @brief Set foreground colour
     *
     * @param self      Object instance
     * @param colour    The new foreground colour in the libuimg defined format
     */
    self->fg_colour = colour;
}

uint32_t lblrdr_get_bg(const lblrdr_t *self)
{
    /**
     * @memberof lblrdr
     * @brief Get current background colour
     */
    return self->bg_colour;
}

void lblrdr_set_bg(lblrdr_t *self, uint32_t colour)
{
    /**
     * @memberof lblrdr
     * @brief Set background colour
     *
     * @param self      Object instance
     * @param colour    The new background colour in the libuimg defined format
     */
    self->bg_colour = colour;
}

static
int calc_glyph_width(lblrdr_t *self, uint32_t code, int *width)
{
    uimg_t *img = fontrdr_gen_glyph(
        self->fonts, code, self->fg_colour, self->font_size, self->font_flags);
    if(!img)
        return ENOMEM;

    *width = uimg_get_width(img);

    uimg_release(img);
    return 0;
}

static
int calc_append_glyph_width(lblrdr_t *self, glyphlist_t *glist, uint32_t code)
{
    int err, width;
    if(!( err = calc_glyph_width(self, code, &width) ))
        err = glyphlist_append(glist, code, width);

    return err;
}

static
int parse_glyph_list(lblrdr_t *self, glyphlist_t *glist, const char *str)
{
    int err = 0;
    for(uint32_t code; !err && ( str = lblrdr_utf8_decode(str, &code) );)
    {
        if( code == '\t' )
        {
            err = glyphlist_append(glist, '\t', 0);
        }
        else if( code == '\n' )
        {
            err = glyphlist_append(glist, code, 0);
        }
        else
        {
            err = calc_append_glyph_width(self, glist, code);
        }
    }

    // Make sure that the string is ended by a line-feed
    if(!err)
    {
        struct glyphinfo *node = glyphlist_last(glist);
        if( node && node->code != '\n' )
            err = glyphlist_append(glist, '\n', 0);
    }

    return err;
}

static
int calc_fill_tab_widths(lblrdr_t *self, glyphlist_t *glist)
{
    int err;

    int space_width;
    if(( err = calc_glyph_width(self, ' ', &space_width) ))
        return err;

    int align_flags = self->layout_flags & LBLRDR_ALIGN_MASK;
    int tab_flags = self->layout_flags & LBLRDR_TAB_MASK;
    int tab_size = self->layout_flags & LBLRDR_TABSIZE_MASK;
    int tab_width = tab_size * space_width;

    if( ( tab_flags & LBLRDR_TAB_AS_SPACE ) ||
        ( align_flags & LBLRDR_ALIGN_CENTRE ) )
    {
        for(struct glyphinfo *node = glyphlist_first(glist);
            !err && node;
            node = glyphlist_next(glist, node))
        {
            if( node->code == '\t' )
                node->width = tab_width;
        }

        return 0;
    }

    int line_width = 0;
    switch(align_flags)
    {
    case LBLRDR_ALIGN_LEFT:
        for(struct glyphinfo *node = glyphlist_first(glist);
            !err && node;
            node = glyphlist_next(glist, node))
        {
            if( node->code == '\n' )
            {
                line_width = 0;
            }
            else if( node->code == '\t' )
            {
                int aligned_width =
                    ( line_width + tab_width ) / tab_width * tab_width;
                node->width = aligned_width - line_width;
                line_width = aligned_width;
            }
            else
            {
                line_width += node->width;
            }
        }
        break;

    case LBLRDR_ALIGN_RIGHT:
        for(struct glyphinfo *node = glyphlist_last(glist);
            !err && node;
            node = glyphlist_prev(glist, node))
        {
            if( node->code == '\n' )
            {
                line_width = 0;
            }
            else if( node->code == '\t' )
            {
                int aligned_width =
                    ( line_width + tab_width ) / tab_width * tab_width;
                node->width = aligned_width - line_width;
                line_width = aligned_width;
            }
            else
            {
                line_width += node->width;
            }
        }
        break;

    case LBLRDR_ALIGN_CENTRE:
        break;

    default:
        err = EINVAL;
        break;
    }

    return err;
}

static
int parse_insert_softwrap(glyphlist_t *glist, bool wrap_anywhere, int max_width)
{
    int err = 0;

    int line_width = 0;
    struct glyphinfo *break_pos = NULL;
    bool prev_is_spacer = false;

    for(struct glyphinfo *node = glyphlist_first(glist);
        !err && node;
        node = glyphlist_next(glist, node))
    {
        switch(node->code)
        {
        case '\n':
            line_width = 0;
            break_pos = NULL;
            prev_is_spacer = false;
            break;

        case '\t':
        case ' ':
            line_width += node->width;
            prev_is_spacer = true;
            break;

        default:
            if( wrap_anywhere || prev_is_spacer )
                break_pos = node;

            line_width += node->width;
            if( line_width > max_width && break_pos )
            {
                err = glyphlist_insert_before(glist, break_pos, '\n', 0);
                line_width = node->width;
                break_pos = NULL;
            }

            prev_is_spacer = false;
            break;
        }
    }

    return err;
}

static
void calc_canvas_size(
    glyphlist_t *glist, int line_h, int *canvas_w, int *canvas_h)
{
    int max_w = 0, curr_w = 0, curr_h = 0;
    for(struct glyphinfo *node = glyphlist_first(glist);
        node;
        node = glyphlist_next(glist, node))
    {
        if( node->code == '\n' )
        {
            curr_w = 0;
            curr_h += line_h;
        }
        else
        {
            curr_w += node->width;
            if( max_w < curr_w )
                max_w = curr_w;
        }
    }

    *canvas_w = max_w;
    *canvas_h = curr_h;
}

static
int render_char(lblrdr_t *self, int x, int y, uint32_t code, uimg_t *canvas)
{
    uimg_t *img = fontrdr_gen_glyph(
        self->fonts, code, self->fg_colour, self->font_size, self->font_flags);
    if(!img)
        return ENOMEM;

    int err = uimgblit_blend(canvas, x, y, img, NULL, UIMG_BLEND_ALPHA);

    uimg_release(img);
    return err;
}

static
int render_left_aligned_line(
    lblrdr_t *self, glyphlist_t *glist, int line_idx, uimg_t *canvas)
{
    int y = line_idx * self->font_size;
    int x = 0;

    int err = 0;
    for(struct glyphinfo *node = glyphlist_first(glist);
        !err && node;
        node = glyphlist_next(glist, node))
    {
        if( node->code != '\t' )
            err = render_char(self, x, y, node->code, canvas);
        x += node->width;
    }

    return err;
}

static
int render_right_aligned_line(
    lblrdr_t *self, glyphlist_t *glist, int line_idx, uimg_t *canvas)
{
    int y = line_idx * self->font_size;
    int x = uimg_get_width(canvas);

    int err = 0;
    for(struct glyphinfo *node = glyphlist_last(glist);
        !err && node;
        node = glyphlist_prev(glist, node))
    {
        x -= node->width;
        if( node->code != '\t' )
            err = render_char(self, x, y, node->code, canvas);
    }

    return err;
}

static
int render_centre_aligned_line(
    lblrdr_t *self, glyphlist_t *glist, int line_idx, uimg_t *canvas)
{
    int line_width = 0;
    for(struct glyphinfo *node = glyphlist_first(glist);
        node;
        node = glyphlist_next(glist, node))
    {
        line_width += node->width;
    }

    int y = line_idx * self->font_size;
    int x = ( uimg_get_width(canvas) - line_width ) / 2;

    int err = 0;
    for(struct glyphinfo *node = glyphlist_first(glist);
        !err && node;
        node = glyphlist_next(glist, node))
    {
        if( node->code != '\t' )
            err = render_char(self, x, y, node->code, canvas);
        x += node->width;
    }

    return err;
}

static
int render_multi_line_glyphs(lblrdr_t *self, glyphlist_t *glist, uimg_t *canvas)
{
    int align_flags = self->layout_flags & LBLRDR_ALIGN_MASK;

    glyphlist_t sublist;
    glyphlist_init(&sublist);

    int err = 0;
    for(int i = 0; !err && !glyphlist_empty(glist); ++i)
    {
        glyphlist_extract_first_line(glist, &sublist);
        switch(align_flags)
        {
        case LBLRDR_ALIGN_LEFT:
            err = render_left_aligned_line(self, &sublist, i, canvas);
            break;

        case LBLRDR_ALIGN_RIGHT:
            err = render_right_aligned_line(self, &sublist, i, canvas);
            break;

        case LBLRDR_ALIGN_CENTRE:
            err = render_centre_aligned_line(self, &sublist, i, canvas);
            break;

        default:
            err = EINVAL;
            break;
        }
    }

    glyphlist_destroy(&sublist);

    return err;
}

uimg_t* lblrdr_render(lblrdr_t *self, const char *str, int max_width)
{
    /**
     * @memberof lblrdr
     * @brief Render string to an image
     *
     * @param self      Object instance
     * @param str       The input string to be rendered
     * @param max_width If this parameter is great than zero, then
     *                  the width of generated image will be limited to
     *                  not exceed this maximum width in pixels; otherwise,
     *                  the image generation will not be limited on width.
     * @return The new generated image on success; or NULL if error occurred.
     */
    if( !str || !str[0] )
        return NULL;
    if(!self->fonts)
        return NULL;

    uimg_t *canvas = NULL;

    glyphlist_t glist;
    glyphlist_init(&glist);

    bool succ = false;
    do
    {
        if(parse_glyph_list(self, &glist, str))
            break;
        if(calc_fill_tab_widths(self, &glist))
            break;

        if( ( self->layout_flags & LBLRDR_SOFTWRAP ) && max_width > 0 )
        {
            parse_insert_softwrap(
                &glist, self->layout_flags & LBLRDR_SOFTWRAP_ANYWHERE, max_width);
        }

        int img_w, img_h;
        calc_canvas_size(&glist, self->font_size, &img_w, &img_h);
        if( max_width > 0 && img_w > max_width )
            img_w = max_width;

        if(!( canvas = uimg_create(img_w, img_h) ))
            break;
        if(uimg_fill(canvas, self->bg_colour))
            break;
        if(render_multi_line_glyphs(self, &glist, canvas))
            break;

        succ = true;
    } while(false);

    glyphlist_destroy(&glist);

    if( !succ && canvas )
    {
        uimg_release(canvas);
        canvas = NULL;
    }

    return canvas;
}
