#ifndef _LABLE_RENDERER_GLYPH_LIST_H_
#define _LABLE_RENDERER_GLYPH_LIST_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include "bsd/list.h"

struct glyphinfo
{
    struct list_head node;

    uint32_t code;
    int width;
};

typedef struct glyphlist
{
    struct list_head head;
} glyphlist_t;

static
void glyphlist_init(glyphlist_t *self)
{
    INIT_LIST_HEAD(&self->head);
}

static
void glyphlist_destroy(glyphlist_t *self)
{
    struct glyphinfo *node, *tmp_node;
    list_for_each_entry_safe(node, tmp_node, &self->head, node)
    {
        free(node);
    }
}

static
void glyphlist_clear(glyphlist_t *self)
{
    struct glyphinfo *node, *tmp_node;
    list_for_each_entry_safe(node, tmp_node, &self->head, node)
    {
        free(node);
    }

    INIT_LIST_HEAD(&self->head);
}

static
bool glyphlist_empty(const glyphlist_t *self)
{
    return list_empty(&self->head);
}

static
int glyphlist_append(glyphlist_t *self, uint32_t code, int width)
{
    struct glyphinfo *node = malloc(sizeof(*node));
    if(!node)
        return ENOMEM;

    node->code = code;
    node->width = width;

    list_add_tail(&node->node, &self->head);
    return 0;
}

static
int glyphlist_insert_before(
    glyphlist_t *self, struct glyphinfo *next, uint32_t code, int width)
{
    struct glyphinfo *node = malloc(sizeof(*node));
    if(!node)
        return ENOMEM;

    node->code = code;
    node->width = width;

    list_add_tail(&node->node, &next->node);
    return 0;
}

static
struct glyphinfo* glyphlist_first(glyphlist_t *self)
{
    return !list_empty(&self->head) ?
        list_entry(self->head.next, struct glyphinfo, node) : NULL;
}

static
struct glyphinfo* glyphlist_last(glyphlist_t *self)
{
    return !list_empty(&self->head) ?
        list_entry(self->head.prev, struct glyphinfo, node) : NULL;
}

static
struct glyphinfo* glyphlist_next(glyphlist_t *self, struct glyphinfo *curr)
{
    return curr->node.next != &self->head ?
        list_entry(curr->node.next, struct glyphinfo, node) : NULL;
}

static
struct glyphinfo* glyphlist_prev(glyphlist_t *self, struct glyphinfo *curr)
{
    return curr->node.prev != &self->head ?
        list_entry(curr->node.prev, struct glyphinfo, node) : NULL;
}

static
void glyphlist_extract_first_line(glyphlist_t *self, glyphlist_t *sublist)
{
    glyphlist_clear(sublist);

    struct glyphinfo *node, *tmp_node;
    list_for_each_entry_safe(node, tmp_node, &self->head, node)
    {
        list_del(&node->node);
        if( node->code == '\n' )
        {
            free(node);
            break;
        }
        else
        {
            list_add_tail(&node->node, &sublist->head);
        }
    }
}

#endif
