#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include "ctest_cmp_img.h"
#include "lblrdr.h"

#ifdef UIMG_USE_SDL
#   include <SDL2/SDL.h>
#endif

#define TEST_FONT_SIZE  32
#define TEST_CACHE_NUM  128
#define TEST_FG_COLOUR  UIMG_COLOUR_BLUE
#define TEST_BG_COLOUR  UIMG_COLOUR_GREEN
#define TEST_WRAP_WIDTH 255

/*
 * Set a suitable (but smallest as possible) image comparison value to
 * avoid the sensitivity problem between different font renderer implementations
 */
#define TEST_COMP_MAXDIFF   0

#define TEST_SAMPLE \
    "# Die unmögliche Tatsache\t--\tMorgenstern\n" \
    "\n" \
    "Weil,\t\tso schließt er messerscharf\n" \
    "Nicht sein kann,\twas nicht sein darf.\n"

static
int setup_test_res(void **state)
{
#ifdef UIMG_USE_SDL
    int err;
    if(( err = SDL_Init(SDL_INIT_VIDEO) ))
        return err;
#endif

    assert_int_equal(system("test -d dump || mkdir dump"), 0);

    lblrdr_t *rdr = malloc(sizeof(*rdr));
    assert_non_null(rdr);
    lblrdr_init(rdr);

    lblrdr_set_font_size(rdr, TEST_FONT_SIZE);
    assert_int_equal(lblrdr_set_font_cache(rdr, TEST_CACHE_NUM), 0);

    lblrdr_set_tab_size(rdr, 8);
    lblrdr_set_fg(rdr, TEST_FG_COLOUR);
    lblrdr_set_bg(rdr, TEST_BG_COLOUR);

    *state = rdr;
    return 0;
}

static
int teardown_test_res(void **state)
{
    lblrdr_t *rdr = *state;
    lblrdr_destroy(rdr);
    free(rdr);

#ifdef UIMG_USE_SDL
    SDL_Quit();
#endif

    return 0;
}

static
void load_fonts(void **state)
{
    lblrdr_t *rdr = *state;
    assert_int_equal(lblrdr_add_font_files(rdr, "fonts/*.ttf"), 0);
}

static
void test_align_left(void **state)
{
    lblrdr_t *rdr = *state;

    int old_flags = lblrdr_get_layout_flags(rdr);
    lblrdr_set_layout_flags(rdr, old_flags | LBLRDR_ALIGN_LEFT);

    uimg_t *img = lblrdr_render(rdr, TEST_SAMPLE, -1);
    assert_non_null(img);

    long diff = load_compare_image("samples/test-align-left.png", img);
    if( diff > TEST_COMP_MAXDIFF )
        uimgfile_save_file(img, "dump/test-align-left.png", 0);
    assert_in_range(diff, 0, TEST_COMP_MAXDIFF);

    uimg_release(img);
    lblrdr_set_layout_flags(rdr, old_flags);
}

static
void test_align_right(void **state)
{
    lblrdr_t *rdr = *state;

    int old_flags = lblrdr_get_layout_flags(rdr);
    lblrdr_set_layout_flags(rdr, old_flags | LBLRDR_ALIGN_RIGHT);

    uimg_t *img = lblrdr_render(rdr, TEST_SAMPLE, -1);
    assert_non_null(img);

    long diff = load_compare_image("samples/test-align-right.png", img);
    if( diff > TEST_COMP_MAXDIFF )
        uimgfile_save_file(img, "dump/test-align-right.png", 0);
    assert_in_range(diff, 0, TEST_COMP_MAXDIFF);

    uimg_release(img);
    lblrdr_set_layout_flags(rdr, old_flags);
}

static
void test_align_centre(void **state)
{
    lblrdr_t *rdr = *state;

    int old_flags = lblrdr_get_layout_flags(rdr);
    lblrdr_set_layout_flags(rdr, old_flags | LBLRDR_ALIGN_CENTRE);

    uimg_t *img = lblrdr_render(rdr, TEST_SAMPLE, -1);
    assert_non_null(img);

    long diff = load_compare_image("samples/test-align-centre.png", img);
    if( diff > TEST_COMP_MAXDIFF )
        uimgfile_save_file(img, "dump/test-align-centre.png", 0);
    assert_in_range(diff, 0, TEST_COMP_MAXDIFF);

    uimg_release(img);
    lblrdr_set_layout_flags(rdr, old_flags);
}

static
void test_softwrap(void **state)
{
    lblrdr_t *rdr = *state;

    int old_flags = lblrdr_get_layout_flags(rdr);
    lblrdr_set_layout_flags(rdr, old_flags | LBLRDR_SOFTWRAP);

    uimg_t *img = lblrdr_render(rdr, TEST_SAMPLE, TEST_WRAP_WIDTH);
    assert_non_null(img);

    long diff = load_compare_image("samples/test-softwrap.png", img);
    if( diff > TEST_COMP_MAXDIFF )
        uimgfile_save_file(img, "dump/test-softwrap.png", 0);
    assert_in_range(diff, 0, TEST_COMP_MAXDIFF);

    uimg_release(img);
    lblrdr_set_layout_flags(rdr, old_flags);
}

static
void test_softwrap_anywhere(void **state)
{
    lblrdr_t *rdr = *state;

    int old_flags = lblrdr_get_layout_flags(rdr);
    lblrdr_set_layout_flags(rdr, old_flags | LBLRDR_SOFTWRAP | LBLRDR_SOFTWRAP_ANYWHERE);

    uimg_t *img = lblrdr_render(rdr, TEST_SAMPLE, TEST_WRAP_WIDTH);
    assert_non_null(img);

    long diff = load_compare_image("samples/test-softwrap-anywhere.png", img);
    if( diff > TEST_COMP_MAXDIFF )
        uimgfile_save_file(img, "dump/test-softwrap-anywhere.png", 0);
    assert_in_range(diff, 0, TEST_COMP_MAXDIFF);

    uimg_release(img);
    lblrdr_set_layout_flags(rdr, old_flags);
}

int main(int argc, char *argv[])
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(load_fonts),
        cmocka_unit_test(test_align_left),
        cmocka_unit_test(test_align_right),
        cmocka_unit_test(test_align_centre),
        cmocka_unit_test(test_softwrap),
        cmocka_unit_test(test_softwrap_anywhere),
    };

    return cmocka_run_group_tests_name(
        "String Renderer Test", tests, setup_test_res, teardown_test_res);
}
