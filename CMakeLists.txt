cmake_minimum_required(VERSION 3.5)
project(lblrdr-master)

option(BUILD_SHARED_LIBS "Build shared library" OFF)
option(BUILD_TEST "Build test applications" ON)

if(${BUILD_SHARED_LIBS})
    message("Library type: Shared")
else()
    message("Library type: Static")
endif()

find_library(LIB_FONTRDR fontrdr)
message("Find fontrdr: ${LIB_FONTRDR}")
if(NOT LIB_FONTRDR)
    message(FATAL_ERROR "Mandatory dependency: fontrdr!")
endif()

find_library(LIB_UIMG uimg)
message("Find uimg: ${LIB_UIMG}")
if(NOT LIB_UIMG)
    message(FATAL_ERROR "Mandatory dependency: uimg!")
endif()

find_library(LIB_SDL SDL2)
message("Find libSDL: ${LIB_SDL}")

find_library(LIB_FREETYPE freetype)
message("Find libfreetype: ${LIB_FREETYPE}")

find_library(LIB_CMOCKA cmocka)
message("Find cmocka: ${LIB_CMOCKA}")

add_subdirectory(lib)
if(BUILD_TEST AND LIB_CMOCKA)
    enable_testing()
    add_subdirectory(test)
endif()
